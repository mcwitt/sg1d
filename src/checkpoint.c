#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "checkpoint.h"
#include "defs.h"
#include "random.h"
#include "sg1d.h"

#define RW_ARRAY(rwfunc, fp, len, i, ffmt, a)\
    for (i = 0; i < len; i++) rwfunc(fp, ffmt " ", a[i])

#define RW_ARRAY_2D(rwfunc, fp, l1, l2, i, j, ffmt, a)\
    for (i = 0; i < l1; i++)\
        for (j = 0; j < l2; j++)\
            rwfunc(fp, ffmt " ", a[i][j])

#define RW_ENSEMBLE(rwfunc, fp, i, j, ffmt, e) {\
    for (i = 0; i < NTMAX; i++)\
        RW_ARRAY(rwfunc, fp, N, j, "%d", e.rep[i].spin);\
    RW_ARRAY(rwfunc, fp, NTMAX, i, "%d", e.rep_at_temp);\
    RW_ARRAY(rwfunc, fp, NTMAX, i, "%d", e.temp_of_rep);\
}

#define RW_CHECKPOINT(rwfunc, fp, i, j, ffmt, s) {\
    RW_ENSEMBLE(rwfunc, fp, i, j, ffmt, s->ens1);\
    RW_ENSEMBLE(rwfunc, fp, i, j, ffmt, s->ens2);\
    RW_RNGSTATE(rwfunc, fp, i, s->rng);\
    RW_ARRAY(rwfunc, fp, NTMAX, i, "%d", s->nswap);\
    RW_ARRAY_2D(rwfunc, fp, NTMAX, N+1, i, j, "%d", s->p);\
    RW_ARRAY_2D(rwfunc, fp, NTMAX, N+1, i, j, ffmt, s->p_av);\
    RW_ARRAY_2D(rwfunc, fp, NTMAX, N+1, i, j, ffmt, s->p2av);\
    RW_ARRAY_2D(rwfunc, fp, NTMAX, NMEAS, i, j, ffmt, s->av);\
    rwfunc(fp, "%d ", s->ioct);\
    rwfunc(fp, "%d ", s->ibin);\
    rwfunc(fp, "%d ", s->istep);\
    rwfunc(fp, "%d ", s->nstep_bin);\
}

static char *file, *backup;

static int save(char *file, sg1d_simstate_t *s);
static int restore(char *file, sg1d_simstate_t *s);

void checkpoint_init(char *fname)
{
    file = fname;
    int len = strlen(CHECKPOINT_BACKUP_SUFFIX);
   
    backup = malloc(len + strlen(file) + 1);
    strcpy(backup, file);
    strcat(backup, CHECKPOINT_BACKUP_SUFFIX);
}

void checkpoint_cleanup(void)
{
    unlink(backup); /* remove backup file */
    free(backup);
}

int checkpoint_save(sg1d_simstate_t *s)
{
    if (*file == '\0') return 0;
    if (save(file, s) || save(backup, s)) return -1;

    return 0;
}

int checkpoint_restore(sg1d_simstate_t *s)
{
    if (*file == '\0') return 0;
    if ((restore(file, s) != 0) && (restore(backup, s) != 0)) return -1;

    return 0;
}

static int save(char *file, sg1d_simstate_t *s)
{
    FILE *fp;
    int i, j;

    if ((fp = fopen(file, "w")) == NULL) return 1;
    RW_CHECKPOINT(fprintf, fp, i, j, "%.16e", s);
    fflush(fp);
    fsync(fileno(fp));
    fclose(fp);

    return 0;
}

static int restore(char *file, sg1d_simstate_t *s)
{
    FILE *fp;
    int i, j;

    if ((fp = fopen(file, "r")) == NULL) return 1;

#define FSCANF(fp, ffmt, dest)\
    if (fscanf(fp, ffmt, &dest) != 1) return -1;

    RW_CHECKPOINT(FSCANF, fp, i, j, "%lf", s);
    if (! feof(fp)) return -1;  /* make sure we've consumed the whole file */
    fclose(fp);

    return 0;
}
