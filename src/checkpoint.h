#include "sg1d.h"

#define CHECKPOINT_BACKUP_SUFFIX ".bak"

void checkpoint_init(char *file);
void checkpoint_cleanup(void);
int checkpoint_save(sg1d_simstate_t *s);
int checkpoint_restore(sg1d_simstate_t *s);
