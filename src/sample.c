#include <assert.h>
#include <math.h>
#include "sample.h"
#include "defs.h"
#include "random.h"
#include "bisect.c"
#include "boxmuller.c"

static int bonded(sample_site_t site[], int i1, int i2);
/* Is there a bond between sites i1 and i2? */

void sample_init(sample_t *p, double sigma, unsigned long seed)
{
    random_t rng;
    sample_site_t *s1, *s2;
    double cdf[N];
    double r, u1, u2, x, sum = 0.;
    int i1, i2, j, nbonds = 0;

    p->energy_ini = 0.;

    for (j = 0; j < N; j++)
    {
        p->site[j].z = 0;
        p->hx2_ini[j] = 0.;
    }

    cdf[0] = 0.;
    for (j = 1; j < N; j++) sum += (cdf[j] = pow(DIST(j), -2.*sigma));
    for (j = 1; j < N; j++) cdf[j] = cdf[j-1] + cdf[j]/sum;

    RANDOM_INIT(&rng, seed);

    while (nbonds < N*Z/2)
    {
        i1 = (int) (N * RANDOM(&rng));
        i2 = (i1 + bisect(cdf, RANDOM(&rng), 1, N)) % N;

        if (bonded(p->site, i1, i2)) continue;

        s1 = &(p->site[i1]);
        s2 = &(p->site[i2]);

        assert((s1->z < ZMAX) && (s2->z < ZMAX));

        s1->neighbor[s1->z] = i2;
        s2->neighbor[s2->z] = i1;

        u1 = RANDOM(&rng);
        u2 = RANDOM(&rng);
        bmgauss(u2, u1, &x, &r);
        r = (float) r;
        s1->jx4[s1->z] = s2->jx4[s2->z] = 4.*r;
        p->hx2_ini[i1] += 2.*r;
        p->hx2_ini[i2] += 2.*r;
        p->energy_ini -= r;

        s1->z++;
        s2->z++;
        nbonds++;
    }
}

static int bonded(sample_site_t site[], int i1, int i2)
{
    int j;

    for (j = 0; j < site[i1].z; j++)
        if (site[i1].neighbor[j] == i2)
            return 1;

    return 0;
}
