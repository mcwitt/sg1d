#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "checkpoint.h"
#include "params.h"
#include "sample.h"
#include "sg1d.h"

#define SG1D_MPI 1

#ifdef SG1D_MPI
#include "mpi.h"
#endif

#define FILE_EXISTS(fname) (access(fname, F_OK) == 0)

double NN = (double) N*N;

sample_t sample;    /* bond configuration */
params_t params;    /* simulation parameters */
sg1d_simstate_t s;  /* state variables to be saved at checkpoint */

/* do a single Monte Carlo step */
#define MCS() {\
    ensemble_sweep(&s.ens1, &sample, params.mbeta, params.nT, &s.rng);\
    ensemble_sweep(&s.ens2, &sample, params.mbeta, params.nT, &s.rng);\
    ensemble_temper(&s.ens1, params.mbeta, s.nswap, params.nT, &s.rng);\
    ensemble_temper(&s.ens2, params.mbeta, s.nswap, params.nT, &s.rng);\
}

int main(int argc, char *argv[])
{
    FILE *datf, *hstf;   /* output files for averages and P(q) data */
    random_seed_t samp_seed;
    replica_t *r1, *r2;
    double x;
    int iT, j, nbin_octave, nstep = 0, nstep_octave, q;

#ifdef SG1D_MPI

    int ntask, rank, rc;

    rc = MPI_Init(&argc, &argv);

    if (rc != MPI_SUCCESS)
    {
        fprintf(stderr, "%s: error starting MPI\n", argv[0]);
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    MPI_Comm_size(MPI_COMM_WORLD, &ntask);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (params_init_mpi(&params, argc, argv, ntask, rank) != 0)
    {
        fprintf(stderr, "%s: error reading parameters\n", argv[0]);
        return EXIT_FAILURE;
    }

#else

    if (params_init(&params, argc, argv) != 0)
    {
        fprintf(stderr, "%s: error reading parameters\n", argv[0]);
        return EXIT_FAILURE;
    }

#endif

    if (params.nT < 2)
    {
        fprintf(stderr, "%s: error: must have 2 or more temperatures\n",
                argv[0]);

        return EXIT_FAILURE;
    }

    if (*params.datfile == '\0') datf = stdout;
    else if ((datf = fopen(params.datfile, WRITE_MODE)) == NULL)
    {
        fprintf(stderr, "%s: error opening file for output %s\n",
                argv[0], params.datfile);

        return EXIT_FAILURE;
    }

    if (*params.hstfile == '\0') hstf = 0;
    else if ((hstf = fopen(params.hstfile, WRITE_MODE)) == NULL)
    {
        fprintf(stderr, "%s: error opening file for output %s\n",
                argv[0], params.hstfile);

        return EXIT_FAILURE;
    }

    samp_seed = atoi(params.samp_seed);
    sample_init(&sample, params.sigma, samp_seed);
    checkpoint_init(params.chkfile);
    nbin_octave = (int) pow(2, LOG2_NBIN_OCTAVE);

    if (FILE_EXISTS(params.chkfile))
    {
        /* attempt to restore saved checkpoint... */

        if (checkpoint_restore(&s) != 0)
        {
            fprintf(stderr, "%s: error reading checkpoint file %s\n",
                    argv[0], params.chkfile);

            return EXIT_FAILURE;
        }

        /* force update of energies and fields */
        for (j = 0; j < params.nT; j++)
        {
            replica_init(&s.ens1.rep[j], &sample);
            replica_init(&s.ens2.rep[j], &sample);
        }

        goto checkpoint_start;
    }

    /* no checkpoint, starting from scratch... */

    /* init random number generator for Monte Carlo */
    RANDOM_INIT(&s.rng, atoi(params.mc_seed));

    /* init parallel tempering replicas in random configurations */
    ensemble_init(&s.ens1, &sample, params.nT, &s.rng);
    ensemble_init(&s.ens2, &sample, params.nT, &s.rng);

    /* do initial equilibration steps */
    for (j = 0; j < nbin_octave; j++) MCS();

    /* print column names */
#define PRINT_COLNAMES(output) fprintf(output,\
        "#%5s %3s %6s %10s %6s "\
        "%8s %14s %14s %14s %14s %8s\n",\
        "N", "z", "sigma", "sample", "oct",\
        "T", "q^2", "q^4", "q_l", "energy", "swap");

    PRINT_COLNAMES(datf);
    if (hstf) PRINT_COLNAMES(hstf);

    /* loop over octaves */

    s.nstep_bin = 1;

    for (s.ioct = LOG2_NBIN_OCTAVE; s.ioct <= params.n_octave; s.ioct++)
    {
        /* reset octave sums */
        for (iT = 0; iT < params.nT; iT++)
        {
            s.nswap[iT] = 0;
            for (j = 0; j < NMEAS; j++) s.av[iT][j] = 0.;
            for (j = 0; j < N+1; j++) s.p_av[iT][j] = 0.;
            for (j = 0; j < N+1; j++) s.p2av[iT][j] = 0.;
        }

        /* loop over bins */
        for (s.ibin = 0; s.ibin < nbin_octave; s.ibin++)
        {
            /* reset bin sums */
            for (iT = 0; iT < params.nT; iT++)
                for (j = 0; j < N+1; j++)
                    s.p[iT][j] = 0;

            for (s.istep = 0; s.istep < s.nstep_bin; s.istep++)
            {
                for (iT = 0; iT < params.nT; iT++)
                {
                    r1 = &s.ens1.rep[s.ens1.rep_at_temp[iT]];
                    r2 = &s.ens2.rep[s.ens2.rep_at_temp[iT]];

                    s.av[iT][QL] += replica_link_overlap(r1, r2, &sample);

                    q = replica_spin_overlap(r1, r2);
                    s.av[iT][Q2] += (double) q*q;
                    s.av[iT][Q4] += (double) q*q*q*q;

                    s.av[iT][ENERGY] += (r1->energy + r2->energy);

                    j = (q + N)/2;
                    s.p[iT][j]++;
                }

                MCS();  /* Monte Carlo step */

                /* create a checkpoint? */
                if ((++nstep) % NSTEP_CHECKPOINT == 0)
                    if (checkpoint_save(&s) != 0)
                        fprintf(stderr, "%s: error saving checkpoint %s\n",
                                argv[0], params.chkfile);

checkpoint_start: ; /* resume saved checkpoints here */
            }

            /* accumulate bin averages */
            for (iT = 0; iT < params.nT; iT++)
            {
                for (j = 0; j < N+1; j++)
                {
                    x = (double) s.p[iT][j] / s.nstep_bin;
                    s.p_av[iT][j] += x;
                    s.p2av[iT][j] += x*x;
                }
            }
        }

        /* normalize averages and print stats for octave */
        nstep_octave = nbin_octave * s.nstep_bin;

        for (iT = 0; iT < params.nT; iT++)
        {
            for (j = 0; j < NMEAS; j++) s.av[iT][j] /= nstep_octave;

#define PRINT_ROW(output) fprintf(output,\
                "%6d %3i %6.3f %10lu %6d "\
                "%8.4f %14e %14e %14e %14e %8.3f",\
                N,\
                Z,\
                params.sigma,\
                (unsigned long) samp_seed,\
                s.ioct,\
                params.T[iT],\
                s.av[iT][Q2] / NN,\
                s.av[iT][Q4] / NN / NN,\
                s.av[iT][QL] / N / Z,\
                0.5 * s.av[iT][ENERGY] / N,\
                50. * s.nswap[iT] / nstep_octave);

            PRINT_ROW(datf); fprintf(datf, "\n"); fflush(datf);

            if (hstf && ((params.n_octave - s.ioct) < N_OCTAVE_FULL_OUTPUT))
            {
                PRINT_ROW(hstf);

                /* print N+1 columns of P(q) data */
                for (j = 0; j < N+1; j++)
                    fprintf(hstf, " %.10e", s.p_av[iT][j] / nbin_octave);

                /* print N+1 columns of P(q) errors */
                for (j = 0; j < N+1; j++)
                {
                    x  = s.p_av[iT][j] / nbin_octave;

                    fprintf(hstf, " %.10e",
                      sqrt((s.p2av[iT][j] / nbin_octave - x*x) /
                        (nbin_octave - 1)));
                }

                fprintf(hstf, "\n");
                fflush(hstf);
            }
        }

        s.nstep_bin *= 2;  /* double the number of steps for next octave */

    }   /* end loop over octaves */

    params_cleanup(&params);
    checkpoint_cleanup();

#ifdef SG1D_MPI
    MPI_Finalize();
#endif

    return EXIT_SUCCESS;
}
