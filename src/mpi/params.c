#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "params.h"

enum
{
    SIGMA,
    N_OCTAVE,
    TFILE,
    DATFILE,
    HSTFILE,
    CHKFILE,
    ICHK,
    SEEDS,
    NPARAM
};

int params_init_mpi(params_t *p, int argc, char *argv[], int ntask, int rank)
{
    FILE *f;
    char *param_name[NPARAM];
    char **param = &argv[1];
    double x;
    int i, ichklen, nseed, seedlen;

    param_name[SIGMA]    = "sigma";
    param_name[N_OCTAVE] = "n_octave";
    param_name[TFILE]    = "temperature file";
    param_name[DATFILE]  = "averages output";
    param_name[HSTFILE]  = "histogram output";
    param_name[CHKFILE]  = "checkpoint file";
    param_name[ICHK]     = "checkpoint index";
    param_name[SEEDS]    = "seed...";

    if (argc < NPARAM+1)
    {
        fprintf(stderr, "%s ", argv[0]);

        for (i = 0; i < NPARAM; i++) 
            fprintf(stderr, "<%s> ", param_name[i]);

        fprintf(stderr, "\n"
                "N                    : %d\n"
                "Z                    : %d\n"
                "ZMAX                 : %d\n"
                "NTMAX                : %d\n"
                "LOG2_NBIN_OCTAVE     : %d\n"
                "N_OCTAVE_FULL_OUTPUT : %d\n"
                "NSTEP_CHECKPOINT     : %d\n",
                N, Z, ZMAX, NTMAX, LOG2_NBIN_OCTAVE, N_OCTAVE_FULL_OUTPUT,
                NSTEP_CHECKPOINT);

        return 1;
    }

    nseed = argc - NPARAM;

    if (ntask != nseed)
    {
        fprintf(stderr, "%s: warning: number of MPI tasks (%d) "
                "does not match the number of seeds provided (%d)\n",
                argv[0], ntask, nseed);

        if (rank >= nseed)
        {
            fprintf(stderr, "%s (%d): warning: seed not specified; exiting\n",
                    argv[0], rank);

            exit(EXIT_SUCCESS);
        }
    }

    p->sigma = atof(param[SIGMA]);
    p->n_octave = atoi(param[N_OCTAVE]);
    p->mc_seed = param[SEEDS + rank];
    p->samp_seed = p->mc_seed;

    seedlen = strlen(p->samp_seed);
    ichklen = strlen(param[ICHK]);

    p->datfile = malloc(strlen(param[DATFILE]) + seedlen + ichklen + 3);
    p->hstfile = malloc(strlen(param[HSTFILE]) + seedlen + ichklen + 3);
    p->chkfile = malloc(strlen(param[CHKFILE]) + seedlen + 2);

    sprintf(p->datfile, "%s.%s.%s", param[DATFILE], p->samp_seed, param[ICHK]);
    sprintf(p->hstfile, "%s.%s.%s", param[HSTFILE], p->samp_seed, param[ICHK]);
    sprintf(p->chkfile, "%s.%s",    param[CHKFILE], p->samp_seed);

    /* read temperatures from file */
    if ((f = fopen(param[TFILE], "r")) == NULL)
    {
        fprintf(stderr, "%s: error opening file %s\n", argv[0], param[TFILE]);
        return 1;
    }

    p->nT = 0;

    while (fscanf(f, "%lf", &x) != EOF)
    {
        if (p->nT == NTMAX)
        {
            fprintf(stderr, "%s: error: temperature list truncated\n", argv[0]);
            return 1;
        }

        p->T[p->nT] = x;
        p->mbeta[p->nT] = -1./x;
        p->nT++;
    }

    return 0;
}

void params_cleanup(params_t *p)
{
    free(p->datfile);
    free(p->hstfile);
    free(p->chkfile);
}
