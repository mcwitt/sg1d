#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "params.h"

enum
{
    SIGMA,
    N_OCTAVE,
    TFILE,
    DATFILE,
    HSTFILE,
    CHKFILE,
    MC_SEED,
    SAMP_SEED,
    NPARAM
};

int params_init(params_t *p, int argc, char *argv[])
{
    FILE *f;
    char *param_name[NPARAM];
    char **param = &argv[1];
    double x;
    int i;

    param_name[SIGMA]     = "sigma";
    param_name[N_OCTAVE]  = "n_octave";
    param_name[TFILE]     = "temperature file";
    param_name[DATFILE]   = "averages output";
    param_name[HSTFILE]   = "histogram output";
    param_name[CHKFILE]   = "checkpoint file";
    param_name[MC_SEED]   = "seed";
    param_name[SAMP_SEED] = "sample";

    if (argc != NPARAM+1)
    {
        fprintf(stderr, "%s ", argv[0]);

        for (i = 0; i < NPARAM; i++) 
            fprintf(stderr, "<%s> ", param_name[i]);

        fprintf(stderr, "\n"
                "N                    : %d\n"
                "Z                    : %d\n"
                "ZMAX                 : %d\n"
                "NTMAX                : %d\n"
                "LOG2_NBIN_OCTAVE     : %d\n"
                "N_OCTAVE_FULL_OUTPUT : %d\n"
                "NSTEP_CHECKPOINT     : %d\n",
                N, Z, ZMAX, NTMAX, LOG2_NBIN_OCTAVE, N_OCTAVE_FULL_OUTPUT,
                NSTEP_CHECKPOINT);

        return 1;
    }

    p->sigma = atof(param[SIGMA]);
    p->n_octave = atoi(param[N_OCTAVE]);
    p->datfile = param[DATFILE];
    p->hstfile = param[HSTFILE];
    p->chkfile = param[CHKFILE];
    p->mc_seed = param[MC_SEED];
    p->samp_seed = param[SAMP_SEED];

    /* read temperatures from file */
    if ((f = fopen(param[TFILE], "r")) == NULL)
    {
        fprintf(stderr, "%s: error opening file %s\n", argv[0], param[TFILE]);
        return 1;
    }

    p->nT = 0;

    while (fscanf(f, "%lf", &x) != EOF)
    {
        if (p->nT == NTMAX)
        {
            fprintf(stderr, "%s: error: temperature list truncated\n", argv[0]);
            return 1;
        }

        p->T[p->nT] = x;
        p->mbeta[p->nT] = -1./x;
        p->nT++;
    }

    return 0;
}

