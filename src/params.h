#include "defs.h"

typedef struct
{
    double sigma;
    int n_octave;
    int nT;
    double T[NTMAX], mbeta[NTMAX];
    char *datfile, *hstfile, *chkfile;  /* output file names */
    char *mc_seed, *samp_seed;
} params_t;

int params_init(params_t *p, int argc, char *argv[]);
int params_init_mpi(params_t *p, int argc, char *argv[], int ntask, int rank);
