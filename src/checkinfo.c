/* program to read checkpoint file and dump status information */

#include <stdio.h>
#include <stdlib.h>
#include "checkpoint.h"

int main(int argc, char *argv[])
{
    sg1d_simstate_t s;

    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s checkpoint_file\n" \
                "List checkpoint information\n", argv[0]);

        return EXIT_FAILURE;
    }

    checkpoint_init(argv[1]);

    if (checkpoint_restore(&s) == -1)
    {
        fprintf(stderr, "Couldn't read checkpoint file `%s'\n", argv[1]);
        return EXIT_FAILURE;
    }

    printf("ioct  = %d\n", s.ioct);
    printf("ibin  = %d\n", s.ibin);
    printf("istep = %d\n", s.istep);

    return EXIT_SUCCESS;
}
