#ifndef SG1D_H
#define SG1D_H

#include "defs.h"
#include "ensemble.h"
#include "random.h"

/* measurements */
enum
{
    Q2,         /* moments of spin overlap */
    Q4,
    QL,         /* link overlap */
    ENERGY,
    SWAP_RATE,  /* parallel tempering replica swap rate */
    NMEAS
};

typedef struct
{
    ensemble_t ens1, ens2;      /* 2 PT ensembles for overlap measurements */
    random_t rng;               /* state of random number generator */
    int nswap[NTMAX];           /* number of replica exchanges */
    int p[NTMAX][N+1];          /* P(q) for current bin */
    double p_av[NTMAX][N+1];    /* bin averages of P(q) */
    double p2av[NTMAX][N+1];    /* bin averages of P^2(q) */
    double av[NTMAX][NMEAS];    /* bin averages of other measurements */
    int ioct, ibin, istep, nstep_bin;
} sg1d_simstate_t;

#endif
