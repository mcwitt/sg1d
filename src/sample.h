#include "defs.h"

#ifndef SAMPLE_H
#define SAMPLE_H

typedef struct
{
    int z;              /* number of bonds */
    int neighbor[ZMAX]; /* list of z bonded sites */
    double jx4[ZMAX];   /* jx4[i] = coupling to ith bonded site times 4 */
} sample_site_t;

typedef struct
{
    sample_site_t site[N];
    double hx2_ini[N];      /* field at each site with all spins up times 2 */
    double energy_ini;      /* energy with all spins up */
} sample_t;

void sample_init(sample_t *s, double sigma, unsigned long seed);

#endif
