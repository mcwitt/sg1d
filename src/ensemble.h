#include "defs.h"
#include "random.h"
#include "replica.h"

typedef struct
{
    replica_t rep[NTMAX];
    int rep_at_temp[NTMAX], temp_of_rep[NTMAX];
} ensemble_t;

void ensemble_init(ensemble_t *e, sample_t *s, int nT, random_t *rng);
void ensemble_sweep(ensemble_t *e, sample_t *s, double mbeta[], int nT, random_t *rng);
void ensemble_temper(ensemble_t *e, double mbeta[], int nswap[], int nT, random_t *rng);

