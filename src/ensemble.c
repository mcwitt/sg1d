#include "ensemble.h"
#include <math.h>

void ensemble_init(ensemble_t *e, sample_t *s, int nT, random_t *rng)
{
    int i;

    for (i = 0; i < nT; i++)
    {
        replica_init_random(&e->rep[i], s, rng);
        e->rep_at_temp[i] = i;
        e->temp_of_rep[i] = i;
    }
}

void ensemble_sweep(ensemble_t *e, sample_t *s, double mbeta[],
        int nT, random_t *rng)
{
    int i;

    for (i = 0; i < nT; i++)
        replica_sweep(&e->rep[i], s, mbeta[e->temp_of_rep[i]], rng);
}

void ensemble_temper(ensemble_t *e, double mbeta[],
        int nswap[], int nT, random_t *rng)
{
    double delta, u;
    int i, swap;

    for (i = 1; i < nT; i++)
    {
        delta = e->rep[e->rep_at_temp[i]].energy -\
                e->rep[e->rep_at_temp[i-1]].energy;

        if (delta < 0. ||
                (u = RANDOM(rng), u < exp(delta * (mbeta[i-1] - mbeta[i]))))
        {
            swap = e->rep_at_temp[i-1];
            e->rep_at_temp[i-1] = e->rep_at_temp[i];
            e->rep_at_temp[i] = swap;

            e->temp_of_rep[e->rep_at_temp[i]] = i;
            e->temp_of_rep[e->rep_at_temp[i-1]] = i-1;

            nswap[i]++;
        }
    }
}

