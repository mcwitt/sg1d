#include "replica.h"
#include <math.h>

void replica_init(replica_t *r, sample_t *p)
{
    int i;

    r->energy = p->energy_ini;
    for (i = 0; i < N; i++) r->hx2[i] = p->hx2_ini[i];

    for (i = 0; i < N; i++)
    {
        if (r->spin[i] == -1)
        {
            r->energy += r->hx2[i];
            replica_update(r, p, i);
        }
    }
}

void replica_init_random(replica_t *r, sample_t *p, random_t *rng)
{
    int i;

    for (i = 0; i < N; i++)
        r->spin[i] = (RANDOM(rng) < 0.5) ? -1 : 1;

    replica_init(r, p);
}

void replica_sweep(replica_t *r, sample_t *p, double mbeta, random_t *rng)
{
    int i;
    double delta, u;

    for (i = 0; i < N; i++)
    {
        delta = r->hx2[i] * r->spin[i];

        if (delta <= 0. || (u = RANDOM(rng), u < exp(mbeta * delta)))
        {
            r->spin[i] *= -1;
            replica_update(r, p, i);
            r->energy += delta;
        }
    }
}

void replica_update(replica_t *r, sample_t *p, int i)
{
    sample_site_t *s = &p->site[i];
    int j;

    /* update field at affected sites */
    if (r->spin[i] == 1)
        for (j = 0; j < s->z; j++)
            r->hx2[s->neighbor[j]] += s->jx4[j];
    else
        for (j = 0; j < s->z; j++)
            r->hx2[s->neighbor[j]] -= s->jx4[j];
}

int replica_spin_overlap(replica_t *r1, replica_t *r2)
{
    int i, q = 0;

    for (i = 0; i < N; i++) q += r1->spin[i] * r2->spin[i];
    return q;
}

int replica_link_overlap(replica_t *r1, replica_t *r2, sample_t *p)
{
    int i1, i2, j, ql = 0;

    for (i1 = 0; i1 < N; i1++)
    {
        for (j = 0; j < p->site[i1].z; j++)
        {
            i2 = p->site[i1].neighbor[j];
            ql += r1->spin[i1] * r1->spin[i2] *\
                  r2->spin[i1] * r2->spin[i2];
        }
    }

    return ql;
}

