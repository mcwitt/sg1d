#include <math.h>

/* Box-Muller transform to generate gaussian random numbers */
static void bmgauss(double u1, double u2, double *r1, double *r2)
{
    double a = sqrt(-2*log(u1));
    *r1 = a*cos(2*M_PI*u2);
    *r2 = a*sin(2*M_PI*u2);
}
