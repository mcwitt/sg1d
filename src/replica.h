#include "defs.h"
#include "random.h"
#include "sample.h"

typedef struct
{
    int spin[N];    /* spin[i] = +/- 1 */
    double hx2[N];  /* local field at each site times 2 */
    double energy;
} replica_t;

void replica_init(replica_t *r, sample_t *p);
void replica_init_random(replica_t *r, sample_t *p, random_t *rng);
void replica_sweep(replica_t *r, sample_t *p, double mbeta, random_t *rng);

/* call to update local fields after flipping spin i */
void replica_update(replica_t *r, sample_t *p, int i);

int replica_spin_overlap(replica_t *r1, replica_t *r2);
int replica_link_overlap(replica_t *r1, replica_t *r2, sample_t *p);

