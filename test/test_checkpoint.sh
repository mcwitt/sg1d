#!/bin/sh

N=32 ../setup
cp "defs.h-test" "defs.h"
make clean sg1d
rm -f "test-histo.dat"
rm -f "test.chk"
./sg1d 0.784 14 "../temp-sets/temps.in-0.784" "" "test-histo.dat" "test.chk" 123 123 > "test-raw.dat"
./sg1d 0.784 15 "../temp-sets/temps.in-0.784" "" "test-histo.dat" "test.chk" 123 123 >> "test-raw.dat"
./sg1d 0.784 16 "../temp-sets/temps.in-0.784" "" "test-histo.dat" "test.chk" 123 123 >> "test-raw.dat"

# filter out duplicate entries
sort -k5 -n "test-raw.dat" | uniq > tmp
mv tmp "test-raw.dat"
diff -q "test-raw.dat" "test-ref-raw.dat" && echo "passed test 1/2" && rm "test-raw.dat"

cat "test-histo.dat" | awk -F' ' '$5>=14' | sort -k5 -n | uniq > tmp
mv tmp "test-histo.dat"
diff -q "test-histo.dat" "test-ref-histo.dat" && echo "passed test 2/2" && rm "test-histo.dat"

rm "test.chk"
