#!/bin/sh

N=32 ../setup
cp "defs.h-test" "defs.h"
make sg1d
rm -f "test-histo.dat"
./sg1d 0.784 16 "../temp-sets/temps.in-0.784" "" "test-histo.dat" "" 123 123 > "test-raw.dat"
diff -q "test-raw.dat"   "test-ref-raw.dat"   && echo "passed test 1/2" && rm "test-raw.dat"
diff -q "test-histo.dat" "test-ref-histo.dat" && echo "passed test 2/2" && rm "test-histo.dat"
