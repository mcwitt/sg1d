import fileinput
from pylab import *

num_q0_vals = 128

files = fileinput.input()
header = files.next().split()   # read header

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_nums = {name: header.index(name) for name in header}
key_names = ['N', 'z', 'sigma', 'oct', 'T']
key_cols = [col_nums[name] for name in key_names]
sample_data = {}

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue   # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    N = int(key[0])
    hist = array(rec[len(header):], dtype=float)
    hist = hist[:N+1]   # chop off errors

    # compute I(q0) for all q0

    Nh = N/2
    s = 0
    iofq0_vals = [0]

    for i in xrange(Nh):
        s += hist[Nh-i-1] + hist[Nh+i]
        iofq0_vals.append(s)

    delta = max(1, len(iofq0_vals) / num_q0_vals)
    sample_data.setdefault(key, []).append(iofq0_vals[::delta])

# print results

print ('# %11s' + (len(key_names)-1)*'%13s') % tuple(key_names)

for key, iofq0_vals in sample_data.iteritems():

    # first index of iofq0_vals runs over samples, second over q0 values

    median_bs = []

    for i in xrange(nsamp):
        samp = iofq0_vals[random_integers(0, len(data)-1, len(data))]
        median_bs.append(median(samp, axis=0))

    iofq0_median = median(iofq0_vals, axis=0)
    iofq0_median_err = std(median_bs)

    output = (len(key_names) * '%13s') % key
    for y in iofq0_median: output += '%13e' % y
    for y in iofq0_median_err: output += '%13e' % y
    print output
