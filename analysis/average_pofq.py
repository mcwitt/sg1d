import fileinput
from pylab import *

files = fileinput.input()
header = files.next().split()   # read header

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_nums = {name: header.index(name) for name in header}
key_names = ['N', 'z', 'sigma', 'oct', 'T']
key_cols = [col_nums[name] for name in key_names]
results = dict()

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue   # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    N = int(rec[col_nums['N']])
    pofq = array(rec[len(header):], dtype=float)
    pofq = pofq[:N+1]*N # chop off errors and normalize

    if key in results:
        results[key][0] += 1  # increment sample count
        results[key][1] += pofq
        results[key][2] += pofq**2
    else:
        results[key] = [1, pofq, pofq**2]

# print results

print ('# %11s' + (len(key_names)-1)*'%13s') % tuple(key_names)

for key, val in results.iteritems():
    nsamp, pofq_sum, p2ofq_sum = val
    pofq = pofq_sum / nsamp
    pofq_err = sqrt(p2ofq_sum / nsamp - pofq**2) / sqrt(nsamp - 1.)
    output = (len(key_names) * '%13s') % key
    for y in pofq: output += '%13e' % y
    for y in pofq_err: output += '%13e' % y
    print output
