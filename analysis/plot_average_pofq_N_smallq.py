from atools import itergroups
from itertools import cycle
from pylab import *

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

files = sys.argv[1:] if len(sys.argv) > 1 else ['average_pofq.npy']

fig = figure()
fig.add_subplot(111)

for f in files:
    data = np.load(f)

    data = data[data['sigma'] == 0.896]

    for ((sigma, N, T), data), marker, color in zip(
            itergroups(data, 'sigma', 'N', 'T'),
            markers, colors):

        q = linspace(-1, 1, len(data['histo'].T))
        plot(q, data['histo'].T, label='$N=%d$' % N)

xlabel('$q$')
ylabel('$P(q)$')
#xlim(-1.1, 1.1)
xlim(-0.2, 0.2)
ylim(0.15, 0.24)
legend(loc='best', title=r'$\sigma=%.3f,\,T=%.2f$' % (sigma, T))

savefig('average_pofq_smallq.eps')
#show()

