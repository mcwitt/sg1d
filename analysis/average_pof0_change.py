import fileinput
from itertools import tee, izip
from pylab import *

def pairwise(iterable):
    's -> (s0,s1), (s1,s2), (s2,s3), ...'
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

files = fileinput.input()
header = files.next().split()   # read header

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_nums = {name: header.index(name) for name in header}

# group data by sample

key_names = ['N', 'z', 'sigma', 'T', 'sample']
key_cols = [col_nums[name] for name in key_names]
pof0s_for_sample = dict()

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue   # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    N = int(rec[col_nums['N']])
    oct = int(rec[col_nums['oct']])
    pofq = rec[len(header):]
    pof0 = float(pofq[N/2]) * N
    pof0s_for_sample.setdefault(key, []).append((oct, pof0))

results = dict()

# average pof0 change over samples

key_names = ['N', 'z', 'sigma', 'T', 'oct']

for (N, z, sigma, T, sample), recs in pof0s_for_sample.iteritems():

    for (oct1, pof01), (oct2, pof02) in pairwise(sorted(recs)):

        key = (N, z, sigma, T, oct2)
        pof0_diff = pof02 - pof01
        
        if key in results:
            results[key][0] += 1  # increment sample count
            results[key][1] += pof0_diff
            results[key][2] += pof0_diff**2
        else:
            results[key] = [1, pof0_diff, pof0_diff**2]

# print results

print ('# %14s' + (len(key_names)-1)*'%16s') % tuple(key_names) \
        + 2*'%16s' % ('pof0_diff', 'pof0_diff_err')

for key, val in results.iteritems():
    nsamp, pof0_diff_sum, pof0_diff_sq_sum = val
    pof0_diff = pof0_diff_sum / nsamp
    pof0_diff_err = sqrt(pof0_diff_sq_sum / nsamp - pof0_diff**2) / sqrt(nsamp - 1.)
    output = (len(key_names) * '%16s') % key
    output += '%16e' % pof0_diff
    output += '%16e' % pof0_diff_err
    print output
