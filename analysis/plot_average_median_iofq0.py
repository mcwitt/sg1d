from itertools import cycle
from glob import glob
from pylab import *

sigma = 0.784

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

dtype = [
        ('N',       'i'),
        ('z',       'i'),
        ('sigma',   'f8'),
        ('oct',     'i'),
        ('T',       'f8')
        ]

fig = figure()
fig.add_subplot(111)

#############################

cmarkers = cycle(markers)
ccolors = cycle(colors)

for f in sorted(glob('average-pofq-s%.3f*' % sigma)):

    data = loadtxt(f, dtype, usecols=range(len(dtype)))
    pofq = loadtxt(f, float)[:, len(dtype):]

    # only plot lowest temperature and last octave
    T = min(data['T'])
    oct = max(data['oct'])
    rows = where((data['T'] == T) & (data['oct'] == oct))
    data = data[rows]
    pofq = pofq[rows]

    N = data['N'][0]
    if N == 64: continue
    Nh = N / 2
    dq0 = 1. / N
    pofq, pofq_err = pofq[:, :N+1], pofq[:, N+1:]

    for data, pofq, pofq_err, marker, color in zip(data, pofq, pofq_err,
            cmarkers, ccolors):

        s = 0
        iofq0 = []

        for i in xrange(Nh):
            s += (pofq[Nh-i] + pofq[Nh+i])*dq0
            iofq0.append(s)

        q0 = linspace(0, 1, Nh)
        plot(q0, iofq0, color=color, label=r'$\langle I(q_0) \rangle,\,N=%d$' % N)

#############################

cmarkers = cycle(markers)
ccolors = cycle(colors)

for f in sorted(glob('median-iofq0-s%.3f*' % sigma)):

    data = loadtxt(f, dtype, usecols=range(len(dtype)))
    iofqs = loadtxt(f, float)[:, len(dtype):]

    # only plot lowest temperature and last octave
    T = min(data['T'])
    oct = max(data['oct'])
    rows = where((data['T'] == T) & (data['oct'] == oct))
    data = data[rows]
    iofqs = iofqs[rows]

    N = data['N'][0]
    if N == 64: continue

    for s, iofq, marker, color in zip(data, iofqs,
            cmarkers, ccolors):

        q = linspace(0, 1, len(iofq))
        plot(q, iofq, color=color, ls='--', label=r'$\~{I}(q_0),\,N=%d$' % N)

##############################

xlabel('$q_0$')
legend(loc='best', ncol=2, title=r'$\sigma=%.3f,\,T=%.2f$' % (data['sigma'], data['T']))

show()

