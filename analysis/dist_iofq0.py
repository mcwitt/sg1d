import fileinput
from itertools import izip
from pylab import *

q0vals = [0.1, 0.2, 0.4]

files = fileinput.input()
header = files.next().split()   # read header

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_nums = {name: header.index(name) for name in header}
key_names = ['N', 'z', 'sigma', 'oct', 'T']
key_cols = [col_nums[name] for name in key_names]
results = dict()

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue   # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    N = int(rec[col_nums['N']])
    pofq = array(rec[len(header):], dtype=float)
    pofq = pofq[:N+1]   # chop off errors and normalize

    # compute I(q0) for all q0

    Nh = N/2
    s = 0
    iofq0s_for_sample = []

    for i in xrange(Nh):
        s += pofq[Nh-i] + pofq[Nh+i]
        iofq0s_for_sample.append(s)

    iofq0s_for_sample = [iofq0s_for_sample[int(Nh*q0)] for q0 in q0vals]
    results.setdefault(key, []).append(iofq0s_for_sample)

# print results

print ('# %11s' + (len(key_names)-1)*'%13s') % tuple(key_names) + '%13s' % 'q0'

for key, iofq0s in results.iteritems():

    # first index of iofq0s runs over samples, second over q0 values
    iofq0s = zip(*iofq0s)   # now first index runs over q0, second over samples

    for q0, iofq0 in izip(q0vals, iofq0s):
        output = (len(key_names) * '%13s') % key + '%5.1f' % q0
        counts, bins = histogram(iofq0, bins=1000, normed=True)
        for y in counts: output += '%13e' % y
        for x in bins: output += '%13e' % x 
        print output
