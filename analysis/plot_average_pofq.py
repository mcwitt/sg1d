from itertools import cycle
from pylab import *

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

dtype = [
        ('N',       'i'),
        ('z',       'i'),
        ('sigma',   'f8'),
        ('oct',     'i'),
        ('T',       'f8')
        ]

fig = figure()
fig.add_subplot(111)

for f in sys.argv[1:]:

    data = loadtxt(f, dtype, usecols=range(len(dtype)))
    pofq = loadtxt(f, float)[:, len(dtype):]

    # only plot lowest temperature and last octave
    T = min(data['T'])
    oct = max(data['oct'])
    rows = where((data['T'] == T) & (data['oct'] == oct))
    data = data[rows]
    pofq = pofq[rows]

    N = data['N'][0]
    pofq, pofq_err = pofq[:, :N+1], pofq[:, N+1:]

    for s, p, p_err, marker, color in zip(data, pofq, pofq_err,
            cycle(markers), cycle(colors)):

        q = linspace(-1, 1, len(p))
        plot(q, p, label=r'$N=%d,\,\sigma=%.3f,\,T=%.2f$' % (N, s['sigma'], s['T']))

xlabel('$q$')
ylabel('$P(q)$')
xlim(-1.1, 1.1)
#xlim(-0.2, 0.2)
legend(loc='best')

show()

