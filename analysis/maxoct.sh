#!/bin/bash

for file in $@; do
    max_octave=$(cat "$file" | grep -v "^#" | awk '\
        BEGIN { max= -1 } \
        $5 > max { max = $5 } \
        END { print max } \
        ')

    #awk -v max=$max_octave 'NR==1 || $5==max' "$file" > "$file.tmp"
    awk -v max=$max_octave '$5==max' "$file" > "$file.tmp"
    mv "$file.tmp" "$file"
done

