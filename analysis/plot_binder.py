from atools import itergroups
from itertools import cycle
from pylab import *

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

files = sys.argv[1:] if len(sys.argv) > 1 else ['data']

fig = figure()
ax = fig.add_subplot(111)

for f in files:
    data = np.load(f)

    for (_, data), marker, color in zip(
            itergroups(data, 'N'),
            markers, colors):

        data = data[data['oct'] == max(data['oct'])]
        sigma = data[0]['sigma']

        ax.errorbar(data['T'], data['g'], data['gerr'],
            label='$%d$' % data[0]['N'],
            color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')


ax.text(0.5, 0.9, r'$\sigma=%.3f$' % sigma,
        horizontalalignment='center',
        transform=ax.transAxes)

ax.set_xlabel('$T$')
ax.set_ylabel('$g$')
ax.legend(loc='best', title='$N$')

show()

