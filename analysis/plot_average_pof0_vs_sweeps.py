from atools import group_rows
from itertools import cycle, izip
from pylab import *

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

dtype = [
        ('N',       'i'),
        ('z',       'i'),
        ('sigma',   'f8'),
        ('oct',     'i'),
        ('T',       'f8')
        ]

fig = figure()
ax = fig.add_subplot(111)

markers = cycle(markers)
colors = cycle(colors)

for f in sys.argv[1:]:

    data = loadtxt(f, dtype, usecols=range(len(dtype)))

    N = data['N'][0]
    pof0_col = len(dtype) + N/2
    pof0_err_col = pof0_col + N
    pof0 = loadtxt(f, float, usecols=[pof0_col, pof0_err_col])

    # only plot lowest temperature
    T = min(data['T'])
    rows = where(data['T'] == T)
    data = data[rows]
    pof0 = pof0[rows]

    # sort data by octave
    p = data.argsort(order='oct')
    data = data[p]
    pof0 = pof0[p]

    pof0, pof0_err = pof0.T

    for ((N, sigma, T), rows), marker, color in izip(
            group_rows(data, 'N', 'sigma', 'T').iteritems(),
            cycle(markers), cycle(colors)):

        errorbar(data['oct'][rows], pof0[rows], pof0_err[rows],
                label=r'$N=%d,\,\sigma=%.3f,\,T=%.2f$' % (N, sigma, T),
                #ls='',
                color=color, marker=marker,
                markeredgecolor=color, markerfacecolor='none')

xlim(21.6, 25.3)
ax.set_xticks(range(22, 26))
xlabel('$\log_2 N_{\mathrm{MCS}}$')
ylabel('$P(0)$')
legend(loc='best')

show()

