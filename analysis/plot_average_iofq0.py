from itertools import cycle
from pylab import *

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

dtype = [
        ('N',       'i'),
        ('z',       'i'),
        ('sigma',   'f8'),
        ('oct',     'i'),
        ('T',       'f8')
        ]

fig = figure()
fig.add_subplot(111)

for f in sys.argv[1:]:

    data = loadtxt(f, dtype, usecols=range(len(dtype)))
    pofq = loadtxt(f, float)[:, len(dtype):]

    # only plot lowest temperature and last octave
    T = min(data['T'])
    oct = max(data['oct'])
    rows = where((data['T'] == T) & (data['oct'] == oct))
    data = data[rows]
    pofq = pofq[rows]

    N = data['N'][0]
    Nh = N / 2
    dq0 = 1. / Nh
    pofq, pofq_err = pofq[:, :N+1], pofq[:, N+1:]

    for data, pofq, pofq_err, marker, color in zip(data, pofq, pofq_err,
            cycle(markers), cycle(colors)):

        s = 0
        iofq0 = []

        for i in xrange(Nh):
            s += (pofq[Nh-i] + pofq[Nh+i])*dq0
            iofq0.append(s)

        q0 = linspace(0, 1, Nh)
        plot(q0, iofq0, label=r'$N=%d$' % N)

xlabel('$q_0$')
ylabel(r'$\langle I(q_0) \rangle$')
legend(loc='best', title=r'$\sigma=%.3f,\,T=%.2f$' % (data['sigma'], data['T']))

show()

