from atools import group_rows
from pylab import *

rc('lines', markersize=9)
rc('font', size=14)

colors = ['b', 'g', 'r', 'm', 'k']

dtype = [
        ('N',       'i'),
        ('z',       'i'),
        ('sigma',   'f8'),
        ('sample',  'i'),
        ('oct',     'i'),
        ('T',       'f8'),
        ('q2',      'f8'),
        ('q4',      'f8'),
        ('ql',      'f8'),
        ('energy',  'f8'),
        ('swap',    'f8')
        ]

fig = figure()
ax = fig.add_subplot(111)

for f in sys.argv[1:]:

    data = loadtxt(f, dtype, usecols=range(len(dtype)))
    pofq = loadtxt(f, float)[:, len(dtype):]
    sigma = data['sigma'][0]
    N = data['N'][0]
    pofq *= N   # normalize

    # only plot data for the lowest temperature from the final octave
    T = min(data['T'])
    oct = max(data['oct'])
    rows = where((data['T'] == T) & (data['oct'] == oct))
    data = data[rows]
    pofq = pofq[rows]
    pofq, pofq_err = pofq[:, :N+1], pofq[:, N+1:]

    q = linspace(-1, 1, N+1)

    for sample, pofq, pofq_err in zip(data['sample'], pofq, pofq_err):
        plot(q, pofq, label='$%d$' % sample)
        #fill_between(q, pofq-pofq_err, pofq+pofq_err, color='b', facecolor='b',
        #    label=sample)

#ax.autoscale()
xlim(-1.2, 1.2)
ylim(0, ylim()[1])
xlabel('$q$')
ylabel('$P(q)$')

legend(loc='best',
       title='$\sigma=%.3f$\n$N=%d$\n$T=%.3f$\n$N_{\mathrm{MCS}}=2^{%d}$' \
                % (sigma, N, T, oct))

show()
