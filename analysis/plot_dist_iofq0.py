from itertools import cycle
from pylab import *

q0 = 0.2
N = 1024

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

dtype = [
        ('N',       'i'),
        ('z',       'i'),
        ('sigma',   'f8'),
        ('oct',     'i'),
        ('T',       'f8'),
        ('q0',      'f8')
        ]

fig = figure()
fig.add_subplot(111)

for f in sys.argv[1:]:

    data = loadtxt(f, dtype, usecols=range(len(dtype)))
    hist = loadtxt(f, float)[:, len(dtype):]

    # only plot lowest temperature and last octave
    T = min(data['T'])
    oct = max(data['oct'])
    rows = where((data['T'] == T) & (data['oct'] == oct) & (data['N'] == N))
    #rows = where((data['T'] == T) & (data['oct'] == oct) & (data['q0'] == q0))
    #rows = where((data['T'] == T) & (data['oct'] == oct) & (data['q0'] == q0) & (data['N'] == N))
    data = data[rows]
    hist = hist[rows]

    nbins = hist.shape[1] / 2
    counts, bins = hist[:, :nbins], hist[:, nbins:]

    for data, counts, bins, marker, color in zip(data, counts, bins,
            cycle(markers), cycle(colors)):

        x = 0.5 * (bins[1:] + bins[:-1])
        plot(x, counts, label=r'$N=%d,\,\sigma=%.3f,\,T=%.2f,\,q_0=%.1f$' \
                % (data['N'], data['sigma'], data['T'], data['q0']))

yscale('log')
xlabel('$I(q_0)$')
ylabel('$P[I(q_0)]$')
legend(loc='best')

show()

