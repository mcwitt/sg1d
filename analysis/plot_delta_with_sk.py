from atools import itergroups
from itertools import cycle
from pylab import *

q0 = 0.2
kappa = 4
sk_data = 'delta-sk-q0.2-k4'

rc('lines', markersize=9)
rc('font', size=14)

markers = cycle(['d', 'o', 's', '^', 'v'])
colors = cycle(['m', 'b', 'g', 'r'])

# plot SK model data

data = genfromtxt(sk_data, names=True, dtype=None)
color = colors.next()

errorbar(data['N'], data['Delta'], data['Delta_err'],
        label='$\mathrm{SK}$',
        #ls='',
        color=color, marker=markers.next(),
        markeredgecolor=color, markerfacecolor='none')

# plot nonzero sigma
fname = sys.argv[1] if len(sys.argv) > 1 else 'delta.dat'
data = np.genfromtxt(fname, names=True, dtype=None)
data = data[(data['q0'] == q0) & (data['kappa'] == kappa)]

for ((sigma,), s), marker, color in zip(
        itergroups(data, 'sigma'),
        markers,
        colors):

    s = s[s['T'] == min(s['T'])]    # only plot lowest temperature

    plot_points = []

    for ((N,), s) in itergroups(s, 'N'):
        s = s[s['oct'] == max(s['oct'])]
        plot_points.append((N, s['Delta'][0], s['Delta_err'][0]))

    N, Delta, Delta_err = zip(*plot_points)

    errorbar(N, Delta, Delta_err,
            label=r'$\sigma=%.3f$' % sigma,
            #ls='',
            color=color, marker=marker,
            markeredgecolor=color, markerfacecolor='none')

xscale('log', basex=2)
xlim(2**5.5, 2**11.5)
#ylim(-0.01, ylim()[1])
ylim(-0.002, ylim()[1])
xlabel('$N$')
ylabel(r'$\Delta(q_0,\,\kappa)$')
legend(loc='best',
        title=r'$q_0=%.1f,\, \kappa=%g$' % (q0, kappa))

show()

