from atools import itergroups
from pylab import *

rc('lines', markersize=9)
rc('font', size=14)

infile = sys.argv[1] if len(sys.argv) > 1 else 'average.npy'
data = np.load(infile)

for (sigma, z, L), s in itergroups(data, 'sigma', 'z', 'N'):

    # only plot lowest temperature
    T = min(s['T'])
    s = s[s['T'] == T]

    u_ql =     0.5 * (s['ql'] - 1) * s['z'] / T
    u_ql_err = 0.5 * s['ql_err']   * s['z'] / T

    fig = figure()
    ax1 = fig.add_subplot(111)
    plotargs = lambda c: dict(color=c, markeredgecolor=c, markerfacecolor='none')

    errorbar(s['oct'], s['energy'], s['energy_err'],
            label='$U$', marker='o', **plotargs('b'))

    errorbar(s['oct'], u_ql, u_ql_err,
           label='$U(q_l)$', marker='s', **plotargs('r'))

    xlabel('$\log_2 N_{\mathrm{MCS}}$')
    legend(loc='best')

    fig = figure()
    ax2 = fig.add_subplot(111)

    axhline(0, ls='--', color='k')
    errorbar(s['oct'], s['dE'], s['dE_err'], marker='o', **plotargs('b'))
    xlabel('$\log_2 N_{\mathrm{MCS}}$')
    ylabel('$u - u(q_l)$')

    for ax in [ax1, ax2]:
        ax.text(0.8, 0.5,
            '$\sigma=%.3f$\n'
            '$L=%d$\n'
            '$T=%.3f$\n'
            '$N_{\mathrm{samp}}=%d$'
            % (sigma, L, T, s[0]['nsamp']),
            horizontalalignment='left',
            verticalalignment='center',
            transform=ax.transAxes)


show()

