#!/bin/bash

for file in $@; do
    min_temp=$(cat "$file" | grep -v "^#" | awk '\
        BEGIN { min = 999 } \
        $6 < min { min = $6 } \
        END { print min } \
        ')

    awk -v min=$min_temp 'NR==1 || $6==min' "$file" > "$file.tmp"
    mv "$file.tmp" "$file"
done
