from atools import itergroups
from itertools import cycle
from pylab import *

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

fname = sys.argv[1] if len(sys.argv) > 1 else 'delta.dat'
data = np.genfromtxt(fname, names=True, dtype=None)

for (q0, kappa), s in itergroups(data, 'q0', 'kappa'):

    fig = figure()
    ax = fig.add_subplot(111)

    for ((sigma,), s), marker, color in zip(
            itergroups(s, 'sigma'),
            markers,
            colors):

        s = s[s['T'] == min(s['T'])]

        plot_points = []

        for ((N,), s) in itergroups(s, 'N'):
            s = s[s['oct'] == max(s['oct'])]
            plot_points.append((N, s['Delta'][0], s['Delta_err'][0]))

        N, Delta, Delta_err = zip(*plot_points)

        errorbar(N, Delta, Delta_err,
                label=r'$\sigma=%.3f$' % sigma,
                #ls='',
                color=color, marker=marker,
                markeredgecolor=color, markerfacecolor='none')

    xscale('log', basex=2)
    xlim(2**6, 2**12)
    xlabel('$N$')
    ylabel(r'$\Delta(q_0,\,\kappa)$')
    legend(loc='best',
            title=r'$q_0=%.1f,\, \kappa=%g$' % (q0, kappa))

show()

