from atools import group_rows
from mpl_toolkits.mplot3d import Axes3D
from pylab import *

infile = sys.argv[1] if len(sys.argv) > 1 else 'histo.dat'

# read header
f = open(infile, 'r')
names = f.next().split()[1:]
nhc = len(names)
f.close()

alldata = genfromtxt(infile, usecols=range(nhc), names=names, dtype=None)
allhist = genfromtxt(infile, dtype='f8')[:, nhc:]

for (sigma, L), rows in group_rows(alldata, 'sigma', 'N').items():

    data = alldata[rows]
    hist = L * allhist[rows]

    # only plot data from the final octave
    oct = max(data['oct'])
    rows = where(data['oct'] == oct)
    data = data[rows]
    hist = hist[rows]

    fig = figure()
    ax = fig.add_subplot(111, projection='3d')

    for (sample,), rows in group_rows(data, 'sample').items():
        samp_data = data[rows]
        samp_hist = hist[rows]

        # extract bin counts and errors
        nbin = samp_hist.shape[1] / 2
        z, err = samp_hist[:, :nbin], samp_hist[:, nbin:]
        q = linspace(-1, 1, nbin)
        x, y = meshgrid(q, samp_data['T'])
        surf = ax.plot_surface(x, y, z, rstride=1, cstride=L/64, cmap=cm.jet)

    ax.text2D(0.03, 0.97,
        '$\sigma=%.3f$\n'
        '$L=%d$\n'
        '$N_{\mathrm{MCS}}=2^{%d}$\n'
        % (sigma, L, oct),
        verticalalignment='top',
        transform=ax.transAxes)

    ax.set_xlabel('$q$')
    ax.set_ylabel('$T$')
    ax.set_zlabel('$P(q)$')
    #fig.colorbar(surf)

    show()
