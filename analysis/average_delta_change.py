import fileinput
import itertools
from itertools import tee, izip
from pylab import *

def pairwise(iterable):
    's -> (s0,s1), (s1,s2), (s2,s3), ...'
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

# parameters

q0vals = [0.2]
kappa_vals = [1., 2., 4.]

param_sets = list(itertools.product(q0vals, kappa_vals))
files = fileinput.input()

# read header

header = files.next().split()

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_num = {name: header.index(name) for name in header}

# group data by sample

key_names = ['N', 'z', 'sigma', 'T', 'sample']
key_cols = [col_num[name] for name in key_names]
sample_peaked = dict()

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue   # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    N = int(rec[col_num['N']])
    ia = N/2
    oct = int(rec[col_num['oct']])
    pofq = rec[len(header):]
    peaked = zeros(len(param_sets))

    for iparams, (q0, kappa) in enumerate(param_sets):

        kappa /= float(N)
        ib = int(ia * (1. + q0))

        # search for peaks
        for y in pofq[ia:ib]:
            if float(y) > kappa:
                peaked[iparams] = 1
                break

    sample_peaked.setdefault(key, []).append((oct, peaked))

results = dict()

# average delta change over samples

key_names = ['N', 'z', 'sigma', 'T', 'oct']

for (N, z, sigma, T, sample), recs in sample_peaked.iteritems():

    for (oct1, peaked1), (oct2, peaked2) in pairwise(sorted(recs)):

        key = (N, z, sigma, T, oct2)
        peaked_diff = peaked2 - peaked1
        
        if key in results:
            results[key][0] += 1  # increment sample count
            results[key][1] += peaked_diff
            results[key][2] += peaked_diff**2   # equals abs(peaked_diff)
        else:
            results[key] = [1, peaked_diff, peaked_diff**2]

# print results

out_names = key_names + ['q0', 'kappa', 'peaked_diff', 'peaked_diff_err']

print ('# %14s' + (len(out_names)-1)*'%16s') % tuple(out_names)

for key, val in results.iteritems():

    nsamp, peaked_diff_sum, peaked_diff_sq_sum = val
    peaked_diff = peaked_diff_sum / nsamp
    peaked_diff_err = sqrt(peaked_diff_sq_sum / nsamp - peaked_diff**2) / sqrt(nsamp - 1.)

    for (q0, kappa), peaked_diff, peaked_diff_err in zip(param_sets, peaked_diff, peaked_diff_err):

        output = (len(key_names) * '%16s') % key
        output += 4*'%16g' % (q0, kappa, peaked_diff, peaked_diff_err)

        print output
