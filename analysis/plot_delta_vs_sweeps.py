from atools import itergroups
from itertools import cycle
from pylab import *

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

fname = sys.argv[1] if len(sys.argv) > 1 else 'delta.dat'
data = np.genfromtxt(fname, names=True, dtype=None)

for (q0, kappa, sigma), s in itergroups(data, 'q0', 'kappa', 'sigma'):

    fig = figure()
    ax = fig.add_subplot(111)

    # only plot lowest temperature
    T = min(s['T'])
    s = s[s['T'] == T]

    for ((N,), s), marker, color in zip(
            itergroups(s, 'N'),
            markers,
            colors):

        errorbar(s['oct'], s['Delta'], s['Delta_err'],
                label=r'$N=%d$' % N,
                #ls='',
                color=color, marker=marker,
                markeredgecolor=color, markerfacecolor='none')

    xlim(21.6, 25.3)
    ax.set_xticks(range(22, 26))
    xlabel('$\log_2 N_{\mathrm{MCS}}$')
    ylabel(r'$\Delta(q_0,\,\kappa)$')
    legend(loc='best',
            title=r'$\sigma=%.3f,\,q_0=%.1f,\,\kappa=%g$' % (sigma, q0, kappa))

show()

