import fileinput
from itertools import tee, izip
from pylab import *

def pairwise(iterable):
    's -> (s0,s1), (s1,s2), (s2,s3), ...'
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

files = fileinput.input()
header = files.next().split()   # read header

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_nums = {name: header.index(name) for name in header}

# group data by sample

key_names = ['N', 'z', 'sigma', 'T', 'sample']
key_cols = [col_nums[name] for name in key_names]
pofqs_for_sample = dict()

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue   # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    N = int(rec[col_nums['N']])
    oct = int(rec[col_nums['oct']])
    pofq = array(rec[len(header):], dtype=float)
    pofq = pofq[:N+1]*N # chop off errors and normalize
    pofqs_for_sample.setdefault(key, []).append((oct, pofq))

results = dict()

# average pofq change over samples
for (N, z, sigma, T, sample), recs in pofqs_for_sample.iteritems():

    for (oct1, pofq1), (oct2, pofq2) in pairwise(sorted(recs)):

        key = (N, z, sigma, T, oct2)
        pofq_diff = pofq2 - pofq1
        
        if key in results:
            results[key][0] += 1  # increment sample count
            results[key][1] += pofq_diff
            results[key][2] += pofq_diff**2
        else:
            results[key] = [1, pofq_diff, pofq_diff**2]

# print results

print ('# %11s' + (len(key_names)-1)*'%13s') % tuple(key_names)

for key, val in results.iteritems():
    nsamp, pofq_diff_sum, pofq_diff_sq_sum = val
    pofq_diff = pofq_diff_sum / nsamp
    pofq_diff_err = sqrt(pofq_diff_sq_sum / nsamp - pofq_diff**2) / sqrt(nsamp - 1.)
    output = (len(key_names) * '%13s') % key
    for y in pofq_diff: output += '%13e' % y
    for y in pofq_diff_err: output += '%13e' % y
    print output
