import sys
import dataformat
from atools import group_stats
from atools.resamp import jackknife_wrapper as jk
from pylab import *

def binder(q2, q4): return 0.5 * (3. - q4 / q2**2)

def dE(energy, ql, T, z): return energy - 0.5 * (ql - 1.) * z / T

if __name__ == '__main__':
    infile  = sys.argv[1] if len(sys.argv) > 1 else 'raw.dat'
    outfile = sys.argv[2] if len(sys.argv) > 2 else 'average'
    data = np.loadtxt(infile, dtype=dataformat.cols)
    mean_err = lambda y: (average(y), std(y) / sqrt(len(y)-1))
    mean_err_f = lambda f: (lambda *y: (average(f(*y)), std(f(*y)) / sqrt(len(y[0])-1)))

    data = group_stats(data, ('sigma', 'z', 'N', 'oct', 'T'),
        stats = [
            ('q2',                          len,        'nsamp'),
            ('ql',                          mean_err,   ('ql', 'ql_err')),
            ('energy',                      mean_err,   ('energy', 'energy_err')),
            (('q2', 'q4'),                  jk(binder), ('g', 'gerr')),
            (('energy', 'ql', 'T', 'z'),    mean_err_f(dE),     ('dE', 'dE_err'))
        ])

    np.save(outfile, data)
