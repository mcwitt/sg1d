from scipy import *
from scipy.integrate import quad
from scipy.optimize import bisect

def lhs(beta):
    return quad(lambda j: exp(-j**2/2.)/sqrt(2*pi)*tanh(beta*j)**2, -Inf, Inf)[0]

def Tc(z):
    return 1./bisect(lambda beta: lhs(beta) - 1./z, 0.1, 10.)

if __name__=='__main__':
    z = input('z=')
    print 'T_c=%f' % Tc(z)
