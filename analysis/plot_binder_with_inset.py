from atools import itergroups
from itertools import cycle
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, mark_inset
from pylab import *

markers = cycle(['o', 's', '^', 'v', 'd', 'h'])
colors = cycle(['b', 'g', 'r', 'm', 'k'])

files = sys.argv[1:] if len(sys.argv) > 1 else ['data']

fig = figure()
axmain = fig.add_subplot(111)

# create inset axes
#axins = inset_axes(axmain, width='45%', height='45%', loc=3)
axins = zoomed_inset_axes(axmain, 2.5, loc=3)

for f in files:
    data = np.load(f)

    for (_, data), marker, color in zip(
            itergroups(data, 'N'),
            markers, colors):

        data = data[data['oct'] == max(data['oct'])]

        for ax in [axmain, axins]:
            ax.errorbar(data['T'], data['g'], data['gerr'],
                label='$%d$' % data[0]['N'],
                color=color, marker=marker,
                markeredgecolor=color, markerfacecolor='none')


axins.set_xlim(1.05, 1.35)
axins.set_ylim(0.47, 0.69)
axins.set_xticks(arange(1.1, 1.4, 0.1))
axins.set_xticks(arange(1.1, 1.4, 0.1))

# hack to put inset x-axis ticks on top, y-axis ticks on right
for axis in [axins.xaxis, axins.yaxis]:
    for tick in axis.iter_ticks():
        tick[0].label2On, tick[0].label1On = True, False

axmain.set_xlabel('$T$')
axmain.set_ylabel('$g$')
axmain.legend(loc='best', title='$N$')

mark_inset(axmain, axins, loc1=2, loc2=4, ec='gray', fc='none')

show()

