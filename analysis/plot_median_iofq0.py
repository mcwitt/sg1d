from itertools import cycle
from pylab import *

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

dtype = [
        ('N',       'i'),
        ('z',       'i'),
        ('sigma',   'f8'),
        ('oct',     'i'),
        ('T',       'f8')
        ]

fig = figure()
fig.add_subplot(111)
sigma = None
T = None

for f in sys.argv[1:]:

    data = loadtxt(f, dtype, usecols=range(len(dtype)))
    iofqs = loadtxt(f, float)[:, len(dtype):]

    sigma = data['sigma'][0]
    N = data['N'][0]

    # only plot lowest temperature and last octave
    T = min(data['T'])
    oct = max(data['oct'])

    rows = where((data['sigma'] == sigma) \
               & (data['T'] == T) \
               & (data['oct'] == oct))

    data = data[rows]
    iofqs = iofqs[rows]

    for iofq, marker, color in zip(iofqs,
            cycle(markers), cycle(colors)):

        q = linspace(0, 1, len(iofq))
        plot(q, iofq, label=r'$N=%d$' % N)

yscale('log')

xlabel('$q$')
ylabel(r'$\~{I}(q)$')
ylim(0, 1)
legend(loc='best', title=r'$\sigma=%.3f,\,T=%.2f$' % (sigma, T))

show()

