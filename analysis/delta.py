import fileinput
import itertools
import sys
from math import *

# parameters

q0vals = [0.2]
kappa_vals = [1., 2., 4.]

param_sets = list(itertools.product(q0vals, kappa_vals))
files = fileinput.input()

# read header

header = files.next().split()

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_num = {name: header.index(name) for name in header}
key_names = ['N', 'z', 'sigma', 'oct', 'T']
key_cols = [col_num[name] for name in key_names]

sample_peaks = dict()

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue       # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    sample_peaks.setdefault(key, [0, [0]*len(param_sets)])[0] += 1
    pofq = rec[len(header):]
    N = int(key[0])
    ia = N/2

    for iparams, (q0, kappa) in enumerate(param_sets):

        kappa /= float(N)
        ib = int(ia * (1. + q0))

        # search for peaks
        for y in pofq[ia:ib]:
            if float(y) > kappa:
                sample_peaks[key][1][iparams] += 1  # increment peak count
                break

# print results

out_names = key_names + ['q0', 'kappa', 'nsamp', 'Delta', 'Delta_err']
print ('# %11s' + (len(out_names)-1)*'%13s') % tuple(out_names)

for key, val in sample_peaks.iteritems():

    nsamp, npeak_list = val

    for (q0, kappa), npeak in zip(param_sets, npeak_list):

        delta = float(npeak) / nsamp
        delta_err = sqrt(npeak) / nsamp

        output = (len(key_names) * '%13s') % key
        output += (2*'%13g' + '%13d' + 2*'%13.9f') \
                % (q0, kappa, nsamp, delta, delta_err)

        print output

