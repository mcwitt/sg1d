import fileinput
import itertools
import sys
from pylab import *

# parameters
q0 = 0.2
kappas = arange(0.1, 4, 0.05)

files = fileinput.input()

# read header

header = files.next().split()

if header[0][0] != '#':
    sys.stderr.write('Missing header row!\n')
    sys.exit(1)

header = header[1:]
col_nums = {name: header.index(name) for name in header}
key_names = ['N', 'z', 'sigma', 'oct', 'T']
key_cols = [col_nums[name] for name in key_names]

peaks = {}

for rec in files:
    rec = rec.split()
    if rec[0][0] == '#': continue   # skip comment lines
    key = tuple(rec[col] for col in key_cols)
    pofq = array(rec[len(header):], float)
    N = int(key[0])
    a = N/2
    b = int(a * (1. + q0))
    peaks.setdefault(key, []).append(max(pofq[a:b]))

# print results

out_names = key_names + ['q0', 'kappa', 'nsamp', 'Delta', 'Delta_err']
print ('# %11s' + (len(out_names)-1)*'%13s') % tuple(out_names)

for key, samp_peaks in peaks.iteritems():

    N = int(key[0])
    nsamp = float(len(samp_peaks))
    samp_peaks = array(samp_peaks)

    for kappa in kappas:
        normed_kappa = kappa / N
        npeak = sum(samp_peaks > normed_kappa)
        delta = npeak / nsamp
        delta_err = sqrt(npeak) / nsamp
        output = (len(key_names) * '%13s') % key
        output += (2*'%13g' + '%13d' + 2*'%13.9f') \
                % (q0, kappa, nsamp, delta, delta_err)

        print output

