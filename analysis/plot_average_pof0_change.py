from atools import itergroups
from itertools import cycle
from pylab import *

markers = ['o', 's', '^', 'v', 'd', 'h']
colors = ['b', 'g', 'r', 'm', 'k']

for f in sys.argv[1:]:

    data = np.genfromtxt(f, names=True, dtype=None)
    fig = figure()
    ax = fig.add_subplot(111)
    axhline(0, ls='--', color='k')

    # only plot lowest temperature
    T = min(data['T'])
    data = data[data['T'] == T]

    for ((N, sigma, T), s), marker, color in zip(
            itergroups(data, 'N', 'sigma', 'T'),
            cycle(markers),
            cycle(colors)):

        errorbar(s['oct'], s['pof0_diff'], s['pof0_diff_err'],
                label=r'$N=%d,\,\sigma=%.3f,\,T=%.2f$' % (N, sigma, T),
                ls='',
                color=color, marker=marker,
                markeredgecolor=color, markerfacecolor='none')

    xlim(21.6, 25.3)
    ax.set_xticks(range(22, 26))
    xlabel('$\log_2 N_{\mathrm{MCS}}$')
    ylabel(r'$\Delta P(0)$')
    legend(loc='best')

show()

