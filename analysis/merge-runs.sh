#!/bin/bash

for f in *.0; do
    root=${f%.0}
    head -n 1 $f > $root    # copy header to new file

    # filter out duplicate data
    cat $root.* | awk '$1 != "#" && !a[$5 "," $6]++' >> $root

    rm $root.*
done
