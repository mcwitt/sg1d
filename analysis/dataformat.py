cols =  [
        ('N',           'i'),
        ('z',           'i'),
        ('sigma',       'f8'),
        ('sample',      'i'),
        ('oct',         'i'),
        ('T',           'f8'),
        ('q2',          'f8'),
        ('q4',          'f8'),
        ('ql',          'f8'),
        ('energy',      'f8'),
        ('swap',        'f8')
        ]
