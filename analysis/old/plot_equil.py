from atools import itergroups
from pylab import *

class EquilPlot:
    def __init__(self, data):
        self.T, self.data = zip(*itergroups(data, 'T'))
        self.iT = 0

    def plot(self, ax):
        d = self.data[self.iT % len(self.data)]
        u_ql = (d['ql'] - 1)*d['z']/(2*d['T'])
        u_ql_err = d['ql_err']*d['z']/(2*d['T'])

        ax.errorbar(d['oct'], d['energy'], d['energy_err'],
                label='$u$', color='b', marker='o',
                markeredgecolor='b', markerfacecolor='none')

        ax.errorbar(d['oct'], u_ql, u_ql_err,
                label='$u^{\prime}(q_l)$', color='g', marker='s',
                markeredgecolor='g', markerfacecolor='none')

        ax.text(0.8, 0.5,
                '$\sigma=%.3f$\n'
                '$N=%d$\n'
                '$T=%.3f$\n'
                '$N_{\mathrm{samp}}=%d$'
                % tuple(d[0][s] for s in ('sigma', 'N', 'T', 'nsamp')),
                horizontalalignment='left',
                verticalalignment='center',
                transform=ax.transAxes)

        ax.set_xlabel('$\log_2 N_{\mathrm{MCS}}$')
        ax.legend(loc='lower right')

if __name__=='__main__':
    infile = sys.argv[1] if len(sys.argv) > 1 else 'data.npy'
    data = np.load(infile)

    for params, data in itergroups(data, 'N', 'z', 'sigma'):
        fig = figure()
        ax = fig.add_subplot(111)

        plot = EquilPlot(data)
        plot.plot(ax)

        def on_key(event):
            if   event.key == 'j': plot.iT -= 1
            elif event.key == 'k': plot.iT += 1
            cla()
            plot.plot(ax)
            draw()

        fig.canvas.mpl_connect('key_press_event', on_key)

    show()
