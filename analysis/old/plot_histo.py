import dataformat
from atools import group_rows
from pylab import *

class HistPlot:
    def __init__(self, stats, data):
        self.stats = stats
        self.data = data
        rowd = group_rows(stats, 'oct')
        keys = rowd.keys()
        keys.sort()
        self.rows = [rowd[key] for key in keys]
        self.ioct = -1
        self.iT = 0
        self.nbin = data.shape[1] / 2

    def plot(self, ax):
        rows = self.rows[self.ioct % len(self.rows)]
        nT = len(rows)
        stats = self.stats[rows][self.iT % nT]
        data = self.data[rows,:][self.iT % nT,:]
        hist = stats['N'] * data[:-self.nbin]
        errs = stats['N'] * data[-self.nbin:]
        q = linspace(-1, 1, self.nbin)
        plot(q, hist)
        #fill_between(q, hist-errs, hist+errs, color='blue', facecolor='blue')

        ax.text(0.03, 0.97,
            '$N=%d$\n'
            '$\sigma=%.3f$\n'
            '$T=%.3f$\n'
            '$N_{\mathrm{MCS}}=2^{%d}$\n'
            % (stats['N'], stats['sigma'], stats['T'], stats['oct']),
            verticalalignment='top',
            transform=ax.transAxes)

        ax.text(0.97, 0.97,
            '%d\n' % stats['sample'],
            verticalalignment='top',
            horizontalalignment='right',
            transform=ax.transAxes)

        ax.autoscale()
        _, ymax = ax.get_ylim()
        ax.set_xlim(-1.4, 1.4)
        ax.set_ylim(0, ymax)
        ax.set_xlabel('$q$')
        ax.set_ylabel('$P(q)$')


if __name__=='__main__':
    infile = sys.argv[1] if len(sys.argv) > 1 else 'histo.dat'
    data = np.loadtxt(infile, dtype='f8')
    nstat = len(dataformat.cols)

    if data.ndim == 1: data.shape = (1, data.shape[0])

    stats = np.rec.fromarrays(data[:,:nstat].T,
        dtype=dataformat.cols)

    hist = data[:, nstat:]

    fig = figure()
    ax = fig.add_subplot(111)

    histplot = HistPlot(stats, hist)
    histplot.plot(ax)

    def on_key(event):
        if   event.key == 'j': histplot.iT -= 1
        elif event.key == 'k': histplot.iT += 1
        elif event.key == 'l': histplot.ioct += 1
        elif event.key == 'h': histplot.ioct -= 1
        cla()
        histplot.plot(ax)
        draw()

    fig.canvas.mpl_connect('key_press_event', on_key)
    show()
